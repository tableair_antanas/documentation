Designator Footprint               Mid X         Mid Y         Ref X         Ref Y         Pad X         Pad Y TB      Rotation Comment        

MP2        9774020243             43.5mm        23.5mm        43.5mm        23.5mm        43.5mm        23.5mm  T          0.00 M2x2mm         
MP1        9774020243              3.5mm        23.5mm         3.5mm        23.5mm         3.5mm        23.5mm  T          0.00 M2x2mm         
C6         0402                   42.9mm        30.3mm        42.9mm        30.3mm        43.4mm        30.3mm  T        180.00 100nF          
C5         0402                   27.7mm        41.9mm        27.7mm        41.9mm        27.7mm        42.4mm  T        270.00 100nF          
C4         0402                    8.8mm        36.6mm         8.8mm        36.6mm         8.3mm        36.6mm  T          0.00 100nF          
C3         0402                      5mm        17.7mm           5mm        17.7mm         4.5mm        17.7mm  T          0.00 100nF          
C2         0402                   19.7mm         5.1mm        19.7mm         5.1mm        19.7mm         4.6mm  T         90.00 100nF          
C1         0402                   37.1mm         9.6mm        37.1mm         9.6mm        36.6mm         9.6mm  T        360.00 100nF          
C8         0402                   34.3mm        29.7mm        34.3mm        29.7mm        34.8mm        29.7mm  T        180.00 100nF          
C52        0402                   31.5mm        34.4mm        31.5mm        34.4mm        31.5mm        33.9mm  T         90.00 100nF          
C20        0402                   18.5mm        10.9mm        18.5mm        10.9mm          18mm        10.9mm  T          0.00 100nF          
C22        0402                 18.525mm        15.3mm      18.525mm        15.3mm      18.025mm        15.3mm  T        360.00 100nF          
C49        0402                   30.3mm         7.5mm        30.3mm         7.5mm        29.8mm         7.5mm  T          0.00 100nF          
C30        0402                   36.3mm        14.2mm        36.3mm        14.2mm        36.3mm        14.7mm  T        270.00 100nF          
C13        0402                   24.7mm        17.1mm        24.7mm        17.1mm        24.7mm        17.6mm  T        270.00 100nF          
C7         0402                   31.3mm        19.4mm        31.3mm        19.4mm        31.8mm        19.4mm  T        180.00 100nF          
C37        0402                   22.7mm          21mm        22.7mm          21mm        22.2mm          21mm  T        360.00 10pF           
C36        0402                   22.6mm        25.1mm        22.6mm        25.1mm        23.1mm        25.1mm  T        180.00 10pF           
C45        0402                     13mm        19.6mm          13mm        19.6mm        12.5mm        19.6mm  T          0.00 2.2uF          
C41        0402                     13mm        20.7mm          13mm        20.7mm        12.5mm        20.7mm  T          0.00 10nF           
C48        0402                   23.9mm        19.3mm        23.9mm        19.3mm        23.9mm        18.8mm  T         90.00 2.2uF          
C43        0402                   22.7mm        19.3mm        22.7mm        19.3mm        22.7mm        18.8mm  T         90.00 10nF           
C11        0402                   20.9mm        19.4mm        20.9mm        19.4mm        20.4mm        19.4mm  T          0.00 100nF          
C10        0402                   24.7mm        28.9mm        24.7mm        28.9mm        24.7mm        29.4mm  T        270.00 100nF          
C9         0402                   23.5mm        28.9mm        23.5mm        28.9mm        23.5mm        28.4mm  T         90.00 2.2uF          
C44        0402                     24mm        27.3mm          24mm        27.3mm        24.5mm        27.3mm  T        180.00 2.2uF          
C40        0402                     24mm        26.2mm          24mm        26.2mm        24.5mm        26.2mm  T        180.00 10nF           
C47        0402                   19.2mm        28.3mm        19.2mm        28.3mm        19.7mm        28.3mm  T        180.00 2.2uF          
C42        0402                   16.3mm        28.3mm        16.3mm        28.3mm        15.8mm        28.3mm  T          0.00 10nF           
FID1       FID                      34mm         3.4mm        35.2mm         4.8mm          34mm         3.4mm  T         90.00 Fiducial coordinates
FID2       FID                       1mm        26.5mm         2.3mm        23.6mm           1mm        26.5mm  T         90.00 Fiducial coordinates
FID3       FID                    40.1mm          39mm        38.7mm          38mm        40.1mm          39mm  T         90.00 Fiducial coordinates
LD1        WS2812B                40.4mm        13.8mm        40.4mm        13.8mm     37.8144mm     12.5215mm  T         60.00 WS2812B        
LD2        WS2812B                23.5mm           4mm        23.5mm           4mm        21.1mm         5.6mm  T        360.00 WS2812B        
LD3        WS2812B                 6.6mm        13.7mm         6.6mm        13.7mm      6.7856mm     16.5785mm  T        300.00 WS2812B        
LD4        WS2812B                 6.6mm        33.3mm         6.6mm        33.3mm      9.1857mm     34.5784mm  T        240.00 WS2812B        
LD5        WS2812B                23.5mm          43mm        23.5mm          43mm        25.9mm        41.4mm  T        180.00 WS2812B        
LD6        WS2812B                40.3mm        33.3mm        40.3mm        33.3mm     40.1144mm     30.4215mm  T        120.00 WS2812B        
R1         R0603R                 41.3mm        19.3mm        41.3mm        19.3mm        41.3mm       20.05mm  T        270.00 200R           
C12        C0603R                  8.9mm        24.2mm         8.9mm        24.2mm         8.9mm       23.45mm  T         90.00 82pF           
C18        C0603R                   12mm          34mm          12mm          34mm       12.75mm          34mm  T        180.00 10pF           
C21        C0603R                18.35mm        9.35mm       18.35mm        9.35mm        17.6mm        9.35mm  T          0.00 10 uF          
C25        C0603R                19.95mm        31.9mm       19.95mm        31.9mm       19.95mm       32.65mm  T        270.00 100pF          
C27        C0603R                 19.2mm        29.6mm        19.2mm        29.6mm       18.45mm        29.6mm  T        360.00 680pF          
C28        C0603R                 19.2mm          34mm        19.2mm          34mm       18.45mm          34mm  T        360.00 15pF           
C31        C0603R                22.35mm        16.9mm       22.35mm        16.9mm        23.1mm        16.9mm  T        180.00 22pF           
C32        C0603R               22.625mm        11.1mm      22.625mm        11.1mm      21.875mm        11.1mm  T          0.00 22pF           
C33        C0603R                16.35mm        29.6mm       16.35mm        29.6mm        15.6mm        29.6mm  T        360.00 680pF          
C35        C0603R                16.35mm          34mm       16.35mm          34mm        15.6mm          34mm  T        360.00 15pF           
C38        C0603R                 15.6mm        31.8mm        15.6mm        31.8mm        15.6mm       32.55mm  T        270.00 100pF          
C39        C0603R                   12mm        35.6mm          12mm        35.6mm       11.25mm        35.6mm  T        360.00 10pF           
C46        C0603R                  7.3mm        24.2mm         7.3mm        24.2mm         7.3mm       24.95mm  T        270.00 82pF           
C50        C0603R                 26.9mm        37.2mm        26.9mm        37.2mm        26.9mm       36.45mm  T         90.00 10 uF          
C51        1206_st                34.4mm     37.0855mm        34.4mm        38.8mm        34.4mm        38.8mm  T        270.00 100uF          
D1         SOD123FA                9.9mm          18mm         9.9mm          18mm        11.3mm          18mm  T        360.00 SS36FA         
L2         L0603                  26.3mm        29.2mm        26.3mm        29.2mm        26.3mm     29.9112mm  T        270.00 BLM18EG471SN1D 
L3         L0603                 19.05mm     13.1112mm       19.05mm     13.1112mm       19.05mm        12.4mm  T         90.00 BLM18EG471SN1D 
L4         L0603                  21.6mm        28.9mm        21.6mm        28.9mm        21.6mm     29.6112mm  T        270.00 270n           
L5         L0603                    14mm      28.925mm          14mm      28.925mm          14mm     29.6362mm  T        270.00 270n           
R2         0603_W                43.75mm          20mm          43mm          20mm          43mm          20mm  T          0.00 CG0603MLC-05E  
R3         0603_W                 44.6mm       27.85mm        44.6mm        28.6mm        44.6mm        28.6mm  T        270.00 CG0603MLC-05E  
R4         0603_W                37.35mm        29.3mm        38.1mm        29.3mm        38.1mm        29.3mm  T        180.00 CG0603MLC-05E  
R5         R0603R               18.625mm     38.1495mm      18.625mm     38.1495mm          19mm        37.5mm  T        120.00 5R6            
R6         R0603R               15.225mm     38.1495mm      15.225mm     38.1495mm        15.6mm        37.5mm  T        120.00 5R6            
R7         R0603R                 14.3mm         7.4mm        14.3mm         7.4mm       13.55mm         7.4mm  T        360.00 47k            
R8         R0603R                   15mm         9.3mm          15mm         9.3mm       15.75mm         9.3mm  T        180.00 47k            
U1         SOT363                 34.4mm          32mm        34.4mm          32mm      33.625mm       32.65mm  T        270.00 74HCT2G04GW    
U2         ST25R3911B-QFN32_5X5_0.5        17.6mm       23.35mm        17.6mm       23.35mm        20.1mm        21.6mm  T        180.00 ST25R3911B     
U3         QFP80P900X900X160-32N        30.4mm        13.5mm        30.4mm        13.5mm       26.25mm        16.3mm  T        360.00 VNC2-32L1C     
U4         SOT23-5               30.17mm     37.3525mm       31.44mm        36.4mm       31.44mm        36.4mm  T        180.00 AP2127K-3.3TRG1
Y1         4-SMD                  22.6mm        23.1mm        22.6mm        23.1mm       22.05mm        23.8mm  T        270.00 Q 27,120-JXS21-10-10/10-FU-WA-LF
Y2         7M                    22.45mm          14mm       22.45mm          14mm        21.6mm        15.1mm  T        270.00 Q-12,0-JXS32-12-30/30-LF
L1         L0603                  42.5mm     27.9888mm        42.5mm     27.9888mm        42.5mm        28.7mm  T        270.00 BLM18EG471SN1D 
C15        0402                   11.1mm        25.1mm        11.1mm        25.1mm        10.6mm        25.1mm  T          0.00 12pF           
C23        0402                   11.1mm          24mm        11.1mm          24mm        10.6mm          24mm  T          0.00 12pF           
C14        0402                   11.1mm        22.9mm        11.1mm        22.9mm        10.6mm        22.9mm  T          0.00 5.6pF          
C19        0402                   11.1mm        21.8mm        11.1mm        21.8mm        10.6mm        21.8mm  T          0.00 5.6pF          
C24        0402                    8.5mm        29.2mm         8.5mm        29.2mm         8.5mm        29.7mm  T        270.00 27pF           
C16        0402                    9.7mm        29.2mm         9.7mm        29.2mm         9.7mm        29.7mm  T        270.00 27pF           
C26        0402                   10.9mm        29.2mm        10.9mm        29.2mm        10.9mm        29.7mm  T        270.00 56pF           
C17        0402                   12.1mm        29.2mm        12.1mm        29.2mm        12.1mm        29.7mm  T        270.00 56pF           
P4         686105183822           29.9mm          27mm        29.9mm          27mm        31.3mm          25mm  T         90.00 686105183822   
R9         R0603R                 33.4mm        21.3mm        33.4mm        21.3mm        33.4mm       22.05mm  T        270.00 10k            
R10        R0603R                 36.7mm        17.7mm        36.7mm        17.7mm       37.45mm        17.7mm  T        180.00 10k            
R11        0603_W                 26.5mm       23.25mm        26.5mm        22.5mm        26.5mm        22.5mm  T         90.00 CG0603MLC-05E  
R12        0603_W                 33.7mm       26.65mm        33.7mm        27.4mm        33.7mm        27.4mm  T        270.00 CG0603MLC-05E  

