#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define PREAMBLE_LENGTH         (1)
#define POSTAMBLE_LENGTH        (1)
#define PRE_POST_AMBLE_LENGTH   (PREAMBLE_LENGTH + POSTAMBLE_LENGTH)
#define PACKET_HEADER_LENGTH    (4)
#define PACKET_FOOTER_LENGTH    (1)
#define PACKET_LENGTH           (PRE_POST_AMBLE_LENGTH + PACKET_HEADER_LENGTH + PACKET_FOOTER_LENGTH)


/* Command list: */
#define GETFIRMWAREVERSION              0xD4, 0x02
#define SETSERIALBAUDRATE_192kbaud      0xD4, 0x10, 0x01
#define SAMCONFIGURATION                0xD4, 0x14, 0x01, 0x00, 0x00
#define INLISTPASSIVETARGET             0xD4, 0x4A, 0x01, 0x00
#define INAUTOPOLL                      0xD4, 0x60, 0xFF, 0x01, 0x00, 0x01, 0x02, 0x03, 0x04

typedef struct {
    void * data;
    size_t length;
} request_t;

const uint8_t pre_post_amble[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
const uint16_t start_of_packet_code = 0xFF00;

uint8_t cg_checksum (uint8_t * data, uint8_t length)
{
    uint8_t checksum = 0;
    for (uint8_t offset = 0; offset < length; offset++)
    {
        checksum -= data[offset];
    }

    return checksum;
}

request_t cg_format (uint8_t * command, uint8_t length)
{
    uint16_t buffer_length = PACKET_LENGTH + length;
    void * buffer = malloc(buffer_length);
    request_t request = { buffer, buffer_length};

    if (buffer)
    {
        memset(buffer, 0, buffer_length);
        uint8_t * offset = (uint8_t *)buffer + PREAMBLE_LENGTH;
        /* Start of Packet Code */
        memcpy(offset, &start_of_packet_code, sizeof(uint16_t));
        offset += sizeof(uint16_t);
        /* Packet Length */
        memcpy(offset, &length, sizeof(uint8_t));
        offset += sizeof(uint8_t);
        /* Packet Length Checksum */
        uint8_t packet_checksum = cg_checksum(&length, sizeof(uint8_t));
        memcpy(offset, &packet_checksum, sizeof(uint8_t));
        offset += sizeof(uint8_t);
        /* Frame Identifier + Packet Data */
        memcpy(offset, command, length);
        offset += length;
        /* Packet Data Checksum */
        uint8_t data_checksum = cg_checksum(command, length);
        memcpy(offset, &data_checksum, sizeof(uint8_t));
        offset += sizeof(uint8_t);
    }

    return request;
}

void cg_echo (uint8_t * data, size_t length)
{
    printf("Formated message:\r\n");
    for (size_t offset = 0; offset < length; offset++)
    {
        printf("%02X%s", data[offset], ((offset + 1) >= length) ? "\r\n" : "");
    }

    printf("Terminal format:\r\n");
    for (size_t offset = 0; offset < length; offset++)
    {
        printf("$%02X%s", data[offset], ((offset + 1) >= length) ? "\r\n" : "");
    }

    printf("Array format:\r\n");
    printf("{ ");
    for (size_t offset = 0; offset < length; offset++)
    {
        printf("%s0x%02X", offset ? ", " : "", data[offset]);
    }
    printf(" };\r\n");
}

const unsigned char cmd_cfg_response[] = { 0x00, 0x00, 0xFF, 0x00, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0x02, 0xFE, 0xD5, 0x15, 0x16, 0x00 };
const unsigned char cmd_ok_response[] = { 0x00, 0x00, 0xFF, 0x00, 0xFF, 0x00 };
const unsigned char cmd_card_read[] = { 0x00, 0x00, 0xFF, 0x0E, 0xF2, 0xD5, 0x61, 0x01, 0x10, 0x09, 0x01, 0x00, 0x04, 0x08, 0x04, 0xAA, 0x35, 0xDC, 0x1D, 0xC7, 0x00 };

/*
0x01, 0x10, 0x09, 0x01, 0x00, 0x04, 0x08, 0x04, 0xAA, 0x35, 0xDC, 0x1D, 0xC7
nbtg 01
type 10
len 09
logic number 01
sens res 00 04
sel res 08
id length 04
id AA 35 DC 1D
*/

static void card_rdr_inautopoll_parse (uint8_t * data, size_t length)
{
    #define CARD_TYPE_MIFARE     (0x10)
    #define MIFARE_UID_4_BYTES   (4)
    #define MIFARE_UID_7_BYTES   (7)

    size_t offset = 0;

    uint8_t nbtg = data[offset++];
    uint8_t type = data[offset++];

    if (type == CARD_TYPE_MIFARE)
    {
        uint8_t ln = data[offset++];
        uint8_t logical_number = data[offset++];
        uint8_t sens_res_msb = data[offset++];
        uint8_t sens_res_lsb = data[offset++];
        uint8_t sel_res = data[offset++];
        uint8_t nfcid_length = data[offset++];
        uint8_t nfcid[nfcid_length];
        memcpy(nfcid, &data[offset], nfcid_length);

        printf("Card length: %u\r\n", nfcid_length);
        if (nfcid_length == MIFARE_UID_4_BYTES)
        {
            printf("Card ID: %02X%02X%02X%02X\r\n", nfcid[0], nfcid[1], nfcid[2], nfcid[3]);
        }
        else if (nfcid_length == MIFARE_UID_7_BYTES)
        {
            printf("Card ID: %02X%02X%02X%02X%02X%02X%02X\r\n", nfcid[0], nfcid[1], nfcid[2], nfcid[3], nfcid[4], nfcid[5], nfcid[6]);
        }
        else
        {
            printf("UID length %u unsupported\r\n", nfcid_length);
        }
    }
    else
    {
        printf("Unsupported card type %u!\r\n", type);
    }
}

static void card_packet_data_parse (uint8_t * data, size_t length)
{
    #define RESPONSE_INAUTOPOLL     (0x61)
    #define RESPONSE_SAMCONFIG      (0x15)

    uint8_t command_code = data[0];
    switch (command_code)
    {
        case RESPONSE_INAUTOPOLL:
        {
            printf("Autopoll response found!\r\n");
            card_rdr_inautopoll_parse(&data[1], length - 1);
        } break;
        case RESPONSE_SAMCONFIG:
        {
            printf("cfg response found!\r\n");
        } break;
        default:
        {
            printf("Unsupported command %02X\r\n", command_code);
        } break;
    }
}

static void card_response_parse (uint8_t * buffer, size_t length)
{
    const uint8_t packet_start[] = { 0x00 /* Preamble */, 0x00, 0xFF /* Start of the packet code */};
    const uint8_t ack_frame[] = { 0x00, 0xFF };
    const uint8_t nack_frame[] = { 0xFF, 0x00 };
    const uint8_t postamble = 0x00;

    size_t offset = 0;

    while (offset < length)
    {
        /* Check current location for packet start */
        if (memcmp(&buffer[offset], packet_start, sizeof(packet_start)) == 0)
        {
            offset += sizeof(packet_start);
            /* Check if this packet is ACK/NACK or data packet */
            if (memcmp(&buffer[offset], ack_frame, sizeof(ack_frame)) == 0)
            {
                offset += sizeof(ack_frame) + sizeof(postamble);
                printf("ACK frame received\r\n");
            }
            else if (memcmp(&buffer[offset], nack_frame, sizeof(nack_frame)) == 0)
            {
                offset += sizeof(nack_frame) + sizeof(postamble);
                printf("NACK frame received\r\n");
            }
            else
            {
                printf("Data frame received\r\n");
                uint8_t len = buffer[offset++];
                uint8_t lcs = buffer[offset++];
                uint8_t tfi = buffer[offset++];
                uint8_t data_length = len - sizeof(tfi);
                uint8_t data[data_length]; /* PD0 to PDn */
                memcpy(data, &buffer[offset], data_length);
                /* Parse actual packet data */
                card_packet_data_parse(data, data_length);

                offset += data_length;
                uint8_t dcs =  buffer[offset++];
                offset += sizeof(postamble);
            }
        }
        else
        {
            printf("Dont know if i need this\r\n");
            break;
        }
    }
}

int main()
{

    uint8_t input[] = { GETFIRMWAREVERSION };
    request_t request = cg_format(input, sizeof(input));
    if (request.data)
    {
        cg_echo((uint8_t *)request.data, request.length);
        free(request.data);
    }
    else
    {
        printf("Format error!\n");
    }


    //card_response_parse(cmd_card_read, sizeof(cmd_card_read));

    return 0;
}
